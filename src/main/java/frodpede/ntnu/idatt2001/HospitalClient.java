package frodpede.ntnu.idatt2001;

public class HospitalClient {
    public static void main(String[] args) {

        Hospital hospital = HospitalTestData.fillRegisterWithTestData(new Hospital("St. Olav"));
        Department department = hospital.departments.get(0);
        Employee firstEmployee = department.employees.get(0);
        Patient notPatient = new Patient("Roald", "Amundsen", "900001");

        try {
            department.remove(firstEmployee);


            department.remove(notPatient);
        } catch (RemoveException e){
            System.out.println(e.getMessage());
        }
    }
}
