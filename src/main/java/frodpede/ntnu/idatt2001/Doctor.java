package frodpede.ntnu.idatt2001;

public abstract class Doctor extends Employee {

    protected Doctor(String firsName, String lastName, String socialSecurityNumber) {
        super(firsName, lastName, socialSecurityNumber);
    }

    public abstract void setDiagnosis(Patient patient, String string);
}
