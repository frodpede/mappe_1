package frodpede.ntnu.idatt2001;

import java.util.ArrayList;
import java.util.Objects;

/**
 * This class includes fields as department name, an arraylist of Patient and Employee.
 *
 */

public class Department {
    private String departmentName;
    public ArrayList<Patient> patients = new ArrayList<>();
    public ArrayList<Employee> employees = new ArrayList<>();

    public Department(String departmentName) {
        this.departmentName = departmentName;
    }

    public void setDepartmentName(String departmentName) {
        if (departmentName.isEmpty()) {
            throw new IllegalArgumentException("Department name is empty");
        }
        this.departmentName = departmentName;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public ArrayList<Patient> getPatients() {
        ArrayList<Patient> arrayList = new ArrayList<>();
        for (Patient patient : patients) {
            arrayList.add(new Patient(patient.getFirstName(),patient.getLastName(),patient.getSocialSecurityNumber()));
        }
        return patients;
    }

    public ArrayList<Employee> getEmployees() {
        ArrayList<Employee> arrayList = new ArrayList<>();
        for (Employee employee : employees) {
            arrayList.add(new Employee(employee.getFirstName(), employee.getLastName(),employee.getSocialSecurityNumber()));
        }
        return employees;
    }

    /**
     *add an empoyee to the list
     * @param employee
     */
    public void addEmployee(Employee employee) {
        Employee newEmployee = new Employee(employee.getFirstName(), employee.getLastName(), employee.getSocialSecurityNumber());
        employees.add(newEmployee);
    }

    /**
     * adds an patient to the list
     * @param patient
     */
    public void addPatient(Patient patient){
        Patient newPatient = new Patient(patient.getFirstName(), patient.getLastName(), patient.getSocialSecurityNumber());
            patients.add(newPatient);
    }

    /**
     * Removes an object of patient or employee
     * @param person is the person being removed
     * @throws RemoveException if the person is not in the register.
     */
    public void remove(Person person) throws RemoveException {
        if (!(employees.contains(person) || patients.contains(person))) {
            throw new RemoveException("Person does not exist in the register.");
        } else if (employees.contains(person)) {
            for (int i = 0; i < employees.size(); i++) {
                if (person.getSocialSecurityNumber().equals(employees.get(i).getSocialSecurityNumber())) {
                    employees.remove(i);
                }
            }

        } else if (patients.contains(person)) {
            for (int i = 0; i < patients.size(); i++) {
                if (person.getSocialSecurityNumber().equals(employees.get(i).getSocialSecurityNumber())) {
                    patients.remove(i);
                }
            }
        }
    }



    @Override
    public int hashCode() {
        return Objects.hash(departmentName, patients, employees);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Department that = (Department) o;
        return departmentName.equals(that.departmentName);
    }

    @Override
    public String toString() {
        return departmentName;
    }
}

