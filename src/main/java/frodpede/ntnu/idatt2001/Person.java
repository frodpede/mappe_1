package frodpede.ntnu.idatt2001;

/**
 * Mappe Del 1
 * @author Frode Pedersen
 *
 * Abstract class for a person
 * It contains fields as first name, last name, and social security number.
 */

public abstract class Person {
    private String firstName;
    private String lastName;
    private String socialSecurityNumber;

    /**
     *
     * @param firstName The first name of the person
     * @param lastName the last name of the person
     * @param socialSecurityNumber The social security number of the person.
     */
    public Person(String firstName, String lastName, String socialSecurityNumber) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.socialSecurityNumber = socialSecurityNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getSocialSecurityNumber() {
        return socialSecurityNumber;
    }

    public String getFullName() {
        return "Name: " + firstName + " " + lastName;
    }

    public void setFirstName(String firstName) {
        if(firstName.isEmpty()) {
            throw new IllegalArgumentException("Firstname is empty");
        }
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        if(lastName.isEmpty()) {
            throw new IllegalArgumentException("Lastname is empty");
        }
        this.lastName = lastName;
    }

    public void setSocialSecurityNumber(String socialSecurityNumber) {
        if(socialSecurityNumber.isEmpty()) {
            throw new IllegalArgumentException("Soscial security number is empty");
        }
        this.socialSecurityNumber = socialSecurityNumber;
    }

    /**
     *
     * @return toString of person
     */
    @Override
    public String toString() {
        return ("Name: " + getFullName() +
                "\nSocial Security Number: " + getSocialSecurityNumber());
    }
}
