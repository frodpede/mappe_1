package frodpede.ntnu.idatt2001;

import java.util.ArrayList;

public class Hospital {
    private final String hospitalName;
    public ArrayList<Department> departments = new ArrayList<>();

    public Hospital(String hospitalName) {
        this.hospitalName = hospitalName;
    }

    public String getHospitalName() {
        return hospitalName;
    }

    public ArrayList<Department> getDepartments() {
        return departments;
    }

    public void addDepartment(Department department) {
        if(!departments.contains(department)) {
            departments.add(department);
        }
    }

    @Override
    public String toString() {
        return "Hospital{" +
                "hospitalName='" + hospitalName + '\'' +
                ", departments=" + departments ;
    }
}
