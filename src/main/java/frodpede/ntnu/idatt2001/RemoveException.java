package frodpede.ntnu.idatt2001;

import java.io.Serial;

public class RemoveException extends Exception {
    @Serial
    private static final long serialVersionUID = 1L;

    public RemoveException(String errorMessage){
        super(errorMessage);
    }
}
