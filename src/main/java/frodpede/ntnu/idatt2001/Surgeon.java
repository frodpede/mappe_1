package frodpede.ntnu.idatt2001;

public class Surgeon extends Doctor{

    public Surgeon(String firstnName, String lastName, String socialSecurityNumber) {
        super(firstnName, lastName, socialSecurityNumber);
    }

    @Override
    public void setDiagnosis(Patient patient, String diagnosis) {
        patient.setDiagnosis(diagnosis);
    }
}
