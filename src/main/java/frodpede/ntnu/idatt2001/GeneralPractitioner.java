package frodpede.ntnu.idatt2001;

public class GeneralPractitioner extends Doctor {

    public GeneralPractitioner(String firstnName, String lastName, String socialSecurityNumber) {
        super(firstnName, lastName, socialSecurityNumber);
    }
    @Override
    public void setDiagnosis(Patient patient, String diagnosis) {
        patient.setDiagnosis(diagnosis);
    }

}
