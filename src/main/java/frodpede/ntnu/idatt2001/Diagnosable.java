package frodpede.ntnu.idatt2001;

public interface Diagnosable {

    void setDiagnosis(String newDiagnosis);
}
